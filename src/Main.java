import java.io.*;

/**
 * DenvosBatProcessor
 * 执行批处理文件的套壳.
 * 开源地址:<a href="https://gitlab.com/Denvo/denvosbatprocessor">Gitlab</a>
 * 更多信息请见Gitlab.
 * @author Denvo Zonis
 * @version 0.0.1
 */
public class Main {
    //获取程序路径.
    private static final String JAR_PATH = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();

    /**
     * 程序入口方法.
     * @param args JVM传入的参数.
     */
    public static void main(String[] args) {
        //处理参数.
        if (args.length == 0) {
            process("null");
        } else if (args.length != 1) {
            System.out.println("参数不正确.请键入\"-help\"获取帮助.");
            System.exit(0);
        } else if ("-help".equals(args[0])) {
            System.out.println("""
                    ==========帮助==========
                    -help 显示本信息.
                    -version 查看版本号.
                    无参数 以默认方式运行批处理文件.
                    文件路径 运行指定的批处理文件.
                    
                    参数只能有0~1个!
                    
                    默认方式:
                    搜寻与程序同目录下的文件名为command的批处理文件(.bat .cmd),然后运行.
                    如果没有找到或者找到多个匹配的文件,则会返回"默认值无效".
                    ========================
                    """);
        } else if ("-version".equals(args[0])) {
            System.out.println("""
                    软件: DenvosBatProcessor
                    作者: Denvo Zonis
                    版本号: 0.0.1
                    """);
        } else {
            process(args[0]);
        }
    }

    /**
     * 运行批处理文件操作.
     * @param arg 指定文件.null即为默认值.
     */
    private static void process(String arg) {
        File commandFile = null;
        if ("null".equals(arg)) {
            //搜索同目录的文件名为command,后缀名为.bat .cmd的文件.
            File batTry = new File(JAR_PATH + "/command.bat");
            File cmdTry = new File(JAR_PATH + "/command.cmd");
            int existCount = 0;
            if (batTry.exists()) {
                commandFile = batTry;
                existCount++;
            }
            if (cmdTry.exists()) {
                commandFile = cmdTry;
                existCount++;
            }
            if (existCount != 1) {
                System.out.println("默认值无效.请键入\"-help\"查看帮助.");
                System.exit(0);
            }
        } else {
            //用户自定义文件路径.
            commandFile = new File(arg);
            if (!commandFile.exists()) {
                System.out.println("批处理文件(.bat .cmd)不存在!指定的文件:\"" + arg + "\"");
                System.exit(0);
            }
        }
        try (InputStream stringIn = Runtime.getRuntime().exec("cmd /c " + commandFile.getPath()).getInputStream()) {
            //执行批处理文件.
            BufferedReader br = new BufferedReader(new InputStreamReader(stringIn));
            String line = br.readLine();
            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}